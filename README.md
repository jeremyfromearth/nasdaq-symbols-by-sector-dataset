# NASDAQ Symbols by Sector Dataset

> A short Python script for downloading stock data for every symbol on the NASDAQ. Data for each sector is downloaded individuallly and saved as a CSV file. Additionally, two other data files are created.

`nasdaq-symbol-index.json`
An lookup of symbol to symbol data

`nasdaq-industry-and-symbols.json`
A heirarchical representation of Sector -> Industry -> Symbol

```
python fetch.py
```
