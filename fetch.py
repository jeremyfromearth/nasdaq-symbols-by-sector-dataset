import csv
import json
import requests
import sys

# Paths for fetching NASDAQ data
base_url = 'https://www.nasdaq.com/screening/companies-by-industry.aspx'

# pagesize is set to 2000 which is high enough to get all symbols for each industry
params = '?pagesize=2000&industry={}&render=download'

# List of values for industry url param
industry = [
  'Basic+Industries',
  'Capital+Goods',
  'Consumer+Durables',
  'Consumer+Non-Durables',
  'Consumer+Services',
  'Energy',
  'Finance',
  'Health+Care',
  'Miscellaneous',
  'Public+Utilities',
  'Technology',
  'Transportation'
]

def add_node(node, data, keyidx, heirarchy):
  '''
    Recursive function for building a heirarchical data structure from tabular data
  '''
  child = None
  for c in node.get('children', []):
    key = data.get(heirarchy[keyidx], None)
    if c.get('name', None) == key:
      child = c

  if not child:
    child = {'name': data[heirarchy[keyidx]], 'parent': node.get('name', 'Untitled'), 'children': []}
    node['children'].append(child)

  if keyidx + 1 < len(heirarchy):
    add_node(child, data, keyidx + 1, heirarchy)

def fetch():
  '''
  Fetches a CSV file for each industry
  Saves the CSV
  Creates an index of symbol name to record and saves it to a file
  Creates a heirarchical representation of the Sector -> Industry -> Symbol and saves it to a file
  '''
  index = {}
  heirarchy = ['Sector', 'Industry', 'Symbol']
  root = {'name': 'root', 'parent': None, 'children': []}

  for i in industry:
    print('Fetching data for Industry: {}'.format(i))
    r = requests.get(base_url + params.format(i))
    lines = r.text.splitlines()
    header = lines[:1][0].replace('"','').split(',')[:-1]
    data = csv.DictReader(lines, fieldnames=header)

    print('Saving CSV file for {}'.format(i))
    k = i.lower().replace('+', '-')
    with open('./data/{}.csv'.format(k), 'w') as f:
      f.write(r.text)

    print('Adding {} to heirarchy'.format(i))
    for i, row in enumerate(data):
      if i > 0: # Ignore the header
        symbol = row['Symbol']
        add_node(root, row, 0, heirarchy)
        index[symbol] = row

  print('Downloads complete with {} symbols added to data'.format(len(index.keys())))

  print('Completed data transforms. Writing index file.')
  with open('./data/nasdaq-symbol-index.json', 'w') as f:
    f.write(json.dumps(index))

  print('Completed data transforms. Writing heirarchical file.')
  with open('./data/nasdaq-industry-and-symbols.json', 'w') as f:
    f.write(json.dumps(root))

if __name__ == '__main__':
  fetch()
